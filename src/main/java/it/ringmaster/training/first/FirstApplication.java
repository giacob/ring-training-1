package it.ringmaster.training.first;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

import static org.springframework.boot.Banner.Mode.*;

@SpringBootApplication
public class FirstApplication {

	public static void main(String[] args) {
		new SpringApplicationBuilder(FirstApplication.class)
				.bannerMode(OFF)
				.build()
				.run(args);
	}
}
