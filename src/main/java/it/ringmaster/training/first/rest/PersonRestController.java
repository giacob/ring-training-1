package it.ringmaster.training.first.rest;

import it.ringmaster.training.first.model.Person;
import it.ringmaster.training.first.repository.PersonRepository;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

/**
 *
 * Custom Spring Controller implementation
 *
 */
@RestController
@RequestMapping("/api/persons")
public class PersonRestController {

	private final PersonRepository personRepository;

	public PersonRestController(PersonRepository personRepository) {
		this.personRepository = personRepository;
	}

	@GetMapping
	public List<Person> findAll() {
		return personRepository.findAll();
	}

	@PostMapping
	public ResponseEntity add(@RequestBody Person entity) {
		Person saved = personRepository.save(entity);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(saved.getId())
				.toUri();
		return ResponseEntity.created(location).build();
	}

	@GetMapping("/{id}")
	public ResponseEntity findById(@PathVariable("id") Long id) {
		return personRepository.findById(id)
				.map(ResponseEntity::ok)
				.orElseGet(ResponseEntity.notFound()::build);
	}

	@PutMapping("/{id}")
	public ResponseEntity update(@PathVariable("id") Long id, @RequestBody Person entity) {
		if (entity == null || (entity.getId() != null && !id.equals(entity.getId()))) {
			return ResponseEntity.badRequest().build();
		}
		if (!personRepository.existsById(id)) {
			return ResponseEntity.notFound().build();
		}
		Person saved = personRepository.save(entity);
		return ResponseEntity.ok(saved);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity deleteById(@PathVariable("id") Long id) {
		try {
			personRepository.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.noContent().build();
	}
}
