package it.ringmaster.training.first.repository;

import it.ringmaster.training.first.model.Person;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 *
 * Custom Spring Data Repository Extension
 *
 * @see <a href="https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repositories.single-repository-behavior">
 *     Customizing Individual Repositories
 *     </a>
 */
public interface PersonCustomRepository {

	List<Person> findByLastNameStartingWithIgnoreCase(String value);

	List<Person> customFindAll();
}
