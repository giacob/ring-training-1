package it.ringmaster.training.first.repository;

import it.ringmaster.training.first.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 *
 * Spring Data Jpa Repository with custom extension declaration and exposed as HATEOAS RestResource by Spring Data Rest
 *
 * @see <a href="https://docs.spring.io/spring-data/jpa/docs/current/reference/html/">
 *     Spring Data Jpa
 *     </a>
 *
 * @see <a href="https://docs.spring.io/spring-data/rest/docs/current/reference/html/">
 *     Spring Data Rest
 *     </a>
 *
 * @see <a href="https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repositories.single-repository-behavior">
 *     Customizing Individual Repositories
 *     </a>
 */
@RepositoryRestResource
public interface PersonRepository extends JpaRepository<Person, Long>, PersonCustomRepository {

	/*
	 * Spring Data implements this "Query Method" from method name conventions
	 * https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods
	 */
	List<Person> findByFirstNameStartingWithIgnoreCase(String value);

	/*
	 * Spring Data implements this custom query method for you
	 * https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.at-query
	 */
	@Query("select p from Person p where length(p.firstName) > :value")
	List<Person> findByFirstNameLength(int value);
}
