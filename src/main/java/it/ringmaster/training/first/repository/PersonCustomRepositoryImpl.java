package it.ringmaster.training.first.repository;

import it.ringmaster.training.first.model.Person;
import it.ringmaster.training.first.model.Person_;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 *
 * Custom Spring Data Repository Extension Implementation
 *
 * @see <a href="https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repositories.single-repository-behavior">
 *     Customizing Individual Repositories
 *     </a>
 */
@Repository
@Transactional(readOnly = true)
public class PersonCustomRepositoryImpl implements PersonCustomRepository {

	private final EntityManager em;

	public PersonCustomRepositoryImpl(EntityManager ent) {
		this.em = ent;
	}

	@Override
	public List<Person> findByLastNameStartingWithIgnoreCase(String value) {
		Assert.hasText(value, "value should be non empty");

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Person> query = cb.createQuery(Person.class);
		Root<Person> r = query.from(Person.class);
		query.where(cb.like(cb.upper(r.get(Person_.lastName)), value.toUpperCase() + "%"));
		return em.createQuery(query).getResultList();
	}

	@Override
	public List<Person> customFindAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Person> query = cb.createQuery(Person.class);
		Root<Person> root = query.from(Person.class);
		query.select(root);
		return em.createQuery(query).getResultList();
	}

}
