'use strict';

App.controller('PersonController', ['$scope', 'Person', function ($scope, Person) {
    var self = this;
    self.person = new Person();

    self.persons = [];

    self.fetchAllPersons = function () {
        self.persons = Person.query();
    };

    self.creatPerson = function () {
        self.person.$save(function () {
            self.fetchAllPersons();
        });
    };

    self.updatePerson = function () {
        self.person.$update(function () {
            self.fetchAllPersons();
        });
    };

    self.deletePerson = function (identity) {
        var person = Person.get({id: identity}, function () {
            person.$delete(function () {
                console.log('Deleting person with id ', identity);
                self.fetchAllPersons();
            });
        });
    };

    self.fetchAllPersons();

    self.submit = function () {
        if (self.person.id == null) {
            console.log('Saving new person', self.person);
            self.creatPerson();
        } else {
            console.log('Updating person with id ', self.person.id);
            self.updatePerson();
        }
        self.reset();
    };

    self.edit = function (id) {
        console.log('id to be edited', id);
        for (var i = 0; i < self.persons.length; i++) {
            if (self.persons[i].id === id) {
                self.person = angular.copy(self.persons[i]);
                break;
            }
        }
    };

    self.remove = function (id) {
        console.log('id to be deleted', id);
        if (self.person.id === id) {
            self.reset();
        }
        self.deletePerson(id);
    };

    self.reset = function () {
        self.person = new Person();
        $scope.personForm.$setPristine();
    };

}]);
