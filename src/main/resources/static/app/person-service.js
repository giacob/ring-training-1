'use strict';


App.factory('Person', ['$resource', function ($resource) {
    return $resource(
        'api/persons/:id',
        {id: '@id'},
        {
            update: {
                method: 'PUT'
            }

        }
    );
}]);